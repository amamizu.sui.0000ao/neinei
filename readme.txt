ねいねいはランダムトークをランダム生成します。

動作環境：Windows10,SSP.2.4.23
動作環境(Mac) : macOS 10.13.x, Wine 3.0(Homebrew), SSP 2.4.23

シェルはゼルチカさんに描いていただきました。

雨水水：http://amamizu.suichu-ka.com/
	（@amamizu_0000ao）

ゼルチカ：https://citringo.net
	Twi: @XelticaWorks


##### 公開日
2018.07.03.

##### 更新履歴
2019.08.15.更新先を変更。
2018.07.24.「ー」を追加。ねいねいジェネレーターをsaori化。
