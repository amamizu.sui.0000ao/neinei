// シェルの動作設定
// 詳細な情報はhttp://ukadoc.googlecode.com/svn/trunk/manual/descript_shell_surfaces.html

//基本設定----------------------------------------------------------------------

charset,Shift_JIS

descript
{
version,1
}
//------------------------------------------------------------------------------
surface0
{
element0,base,surface0.png,0,0
element1,overlay,surface2.png,0,0
element2,overlay,surface3.png,0,0

//まばたき
animation0.interval,sometimes
animation0.pattern0,overlay,4,80,0,0,
animation0.pattern1,overlay,-1,80,0,0

//口パク
animation1.interval,talk,2
animation1.pattern0,overlay,1,80,0,0,
animation1.pattern1,overlay,-1,80,0,0

collision0,180,131,225,216,mode
}

surface10
{
}
